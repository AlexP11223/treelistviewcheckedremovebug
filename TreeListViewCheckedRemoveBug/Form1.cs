﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace TreeListViewCheckedRemoveBug
{
    public partial class Form1 : Form
    {
        class Employee
        {
            public string Name { get; set; }

            public override string ToString() => Name;
        }

        public Form1()
        {
            InitializeComponent();

            var data = new List<Employee>
            {
                new Employee { Name = "Alice"},
                new Employee { Name = "Bob"},
                new Employee { Name = "Carol"},
            };

            tvEmployees.Roots = data;

            UpdateControlsState();
        }

        private void UpdateControlsState()
        {
            btnRemove.Enabled = tvEmployees.CheckedObjects.Count > 0;
        }

        private void tvEmployees_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            UpdateControlsState();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            var objects = tvEmployees.CheckedObjects;

            foreach (Employee obj in objects)
            {
                if (MessageBox.Show($"Do you want to remove {obj}?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                    return;

                tvEmployees.RemoveObject(obj);
            }

            UpdateControlsState();
        }
    }
}
