﻿namespace TreeListViewCheckedRemoveBug
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tvEmployees = new BrightIdeasSoftware.TreeListView();
            this.btnRemove = new System.Windows.Forms.Button();
            this.colName = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            ((System.ComponentModel.ISupportInitialize)(this.tvEmployees)).BeginInit();
            this.SuspendLayout();
            // 
            // tvEmployees
            // 
            this.tvEmployees.AllColumns.Add(this.colName);
            this.tvEmployees.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tvEmployees.CellEditUseWholeCell = false;
            this.tvEmployees.CheckBoxes = true;
            this.tvEmployees.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colName});
            this.tvEmployees.Location = new System.Drawing.Point(12, 67);
            this.tvEmployees.Name = "tvEmployees";
            this.tvEmployees.ShowGroups = false;
            this.tvEmployees.Size = new System.Drawing.Size(307, 480);
            this.tvEmployees.TabIndex = 0;
            this.tvEmployees.UseCompatibleStateImageBehavior = false;
            this.tvEmployees.View = System.Windows.Forms.View.Details;
            this.tvEmployees.VirtualMode = true;
            this.tvEmployees.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.tvEmployees_ItemChecked);
            // 
            // btnRemove
            // 
            this.btnRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.btnRemove.ForeColor = System.Drawing.Color.Red;
            this.btnRemove.Location = new System.Drawing.Point(12, 13);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(48, 48);
            this.btnRemove.TabIndex = 4;
            this.btnRemove.Text = "➖";
            this.btnRemove.UseVisualStyleBackColor = true;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click);
            // 
            // colName
            // 
            this.colName.AspectName = "Name";
            this.colName.FillsFreeSpace = true;
            this.colName.Text = "Name";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 559);
            this.Controls.Add(this.btnRemove);
            this.Controls.Add(this.tvEmployees);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.tvEmployees)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private BrightIdeasSoftware.TreeListView tvEmployees;
        private System.Windows.Forms.Button btnRemove;
        private BrightIdeasSoftware.OLVColumn colName;
    }
}

